/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  swcMinify: true,
  compiler: {
    styledComponents: true,
  },
  serverRuntimeConfig: {},
  publicRuntimeConfig: {
    APP_URL: process.env.APP_URL || 'http://localhost:3000',
    API_URL: process.env.API_URL || 'http://localhost:3000/api',
  },
  images: {
    domains: ['d35xwkx70uaomf.cloudfront.net'],
  },
};
