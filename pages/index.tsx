import { FC } from 'react';
import { GetStaticProps } from 'next';

import { HomePage } from 'app/Home';
import { getAllCaravans } from 'api/calls/getAllCaravans';
import { CaravanType } from 'api/types/caravan';

type PageProps = {
  data: {
    items: CaravanType[];
    count: number;
  };
};

const Page: FC<PageProps> = ({ data }) => {
  return <HomePage data={data} />;
};

export const getStaticProps: GetStaticProps = async () => {
  // TODO add mapping and check of url for filter
  const data = await getAllCaravans();

  return { props: { data } };
};

export default Page;
