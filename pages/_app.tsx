import { FC, StrictMode } from 'react';
import type { AppProps } from 'next/app';

import { GlobalStyle } from 'common/styles/GlobalStyles';
import { ThemeProvider } from 'common/styles/themes/ThemeProvider';
import { QueryClientProvider } from 'api/query-client/QueryClientProvider';

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <StrictMode>
      <GlobalStyle />
      <ThemeProvider>
        <QueryClientProvider>
          <Component {...pageProps} />
        </QueryClientProvider>
      </ThemeProvider>
    </StrictMode>
  );
};

export default MyApp;
