import libAxios from 'axios';
import getConfig from 'next/config';

const {
  publicRuntimeConfig: { API_URL },
} = getConfig();

export const axios = libAxios.create({
  baseURL: API_URL,
});
