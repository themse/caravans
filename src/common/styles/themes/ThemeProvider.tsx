import { FC } from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import { defaultTheme } from './themes';

export const ThemeProvider: FC = ({ children }) => {
  return (
    <StyledThemeProvider theme={defaultTheme}>{children}</StyledThemeProvider>
  );
};
