import { DefaultTheme } from 'styled-components';

import { pxToRem } from 'common/styles/helpers';

export const DEFAULT_FONT_SIZE = 16;

export const defaultTheme: DefaultTheme = {
  screens: {
    mobile: '320px',
    desktop: '1240px',
  },

  colors: {
    orange: '#FF5E55',
    beige: '#EDEAE3',
    white: '#FFFFFF',
    darkBlue: '#1F2244',
    darkGrey: '#9C8C8C',
    green: '#119383',
  },

  fontSize: {
    sm: pxToRem(12),
    md: pxToRem(14),
    lg: pxToRem(16),
    xl: pxToRem(24),
  },

  fontFace: {
    primary: {
      fontFamily: 'Roboto',
      fontWeight: 400,
      fontStyle: 'normal',
    },
    heading: {
      fontFamily: 'Roboto',
      fontWeight: 700,
      fontStyle: 'normal',
    },
  },
};
