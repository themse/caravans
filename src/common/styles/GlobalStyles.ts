import * as styled from 'styled-components';
import { normalize } from 'polished';

export const GlobalStyle = styled.createGlobalStyle`
  ${normalize()}

  html {
    box-sizing: border-box;
  }

  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  body {
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
  }

  p,
  h1,
  h2 {
    margin: 0;
  }
`;
