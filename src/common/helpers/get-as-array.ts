export const getAsArray = <T = unknown>(value: T | T[]): Array<T> =>
  Array.isArray(value) ? value : [value];
