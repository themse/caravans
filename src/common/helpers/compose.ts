export const compose =
  <T extends (arg: U) => U, U>(...fns: T[]) =>
  (data: U) =>
    fns.reduceRight((acc, fn) => fn(acc), data);
