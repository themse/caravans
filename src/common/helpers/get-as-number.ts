import { getAsString } from 'common/helpers/get-as-string';

export const getAsNumber = (
  value: string | string[],
  defaultValue?: number
): number => {
  const result = +getAsString(value);
  if (defaultValue) {
    return Number.isNaN(result) ? defaultValue : result;
  }
  return result;
};
