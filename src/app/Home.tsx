import { FC } from 'react';

import { BaseLayout } from 'app/components/layout/BaseLayout';
import { Container } from 'components/Container';
import { CardList } from 'app/components/CardList';
import { CaravanType } from 'api/types/caravan';
import { Filter } from 'app/components/filter/Filter';
import { useFilterCollection } from 'app/hooks/filter-collection/useFilterCollection';
import { Alert } from 'components/Alert';

type HomePageProps = {
  data: {
    items: CaravanType[];
    count: number;
  };
};

export const HomePage: FC<HomePageProps> = ({ data: initialData }) => {
  const { items, isMore, onLoadMore } = useFilterCollection(initialData);

  return (
    <BaseLayout title="Caravans">
      <Filter />
      <Container>
        {items?.length > 0 ? (
          <CardList items={items} isMore={isMore} onLoadMore={onLoadMore} />
        ) : (
          <Alert>No caravan found</Alert>
        )}
      </Container>
    </BaseLayout>
  );
};
