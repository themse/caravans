import { CaravanType } from 'api/types/caravan';
import { getAsArray } from 'common/helpers/get-as-array';
import { FieldNameType } from './useFilterVehicleType';

export const getFromQueryVehicleType = (
  allowedList: FieldNameType[],
  params?: FieldNameType[]
) => {
  if (params) {
    return getAsArray(params).filter((item) => allowedList.includes(item));
  }

  return [];
};

export const getAllowedList = (data: CaravanType[]) => {
  return [...new Set(data.map((item) => item.vehicleType.toLowerCase()))];
};
