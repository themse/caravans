import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { VehicleType } from 'api/types/vehicle';
import { useVehicleType } from 'app/hooks/useVehicleType';
import { VehicleTypeOption } from 'types/filter';
import { getFromQueryVehicleType } from 'app/hooks/filter-options/type/helpers';

export type FieldNameType = Lowercase<VehicleType>;

export const useFilterVehicleType = () => {
  const { typeList: allowedList } = useVehicleType();

  const router = useRouter();
  const [fromQueryVehicleType, setFromQueryVehicleType] = useState<
    FieldNameType[]
  >([]);

  const changeQueryParams = (type: FieldNameType[]) => {
    router.push(
      {
        pathname: '/',
        query: { ...router.query, type },
      },
      undefined,
      { shallow: true }
    );
  };

  const onChangeVehicleType = (options: VehicleTypeOption) => {
    const { isChecked } = options;
    const name = options.name as FieldNameType;

    if (!allowedList.includes(name)) {
      return;
    }

    if (isChecked) {
      return setFromQueryVehicleType((prev) => {
        const newState = [...new Set([...prev, name])];
        changeQueryParams(newState);

        return newState;
      });
    }
    return setFromQueryVehicleType((prev) => {
      const newState = prev.filter((item) => item !== name);
      changeQueryParams(newState);

      return newState;
    });
  };

  useEffect(() => {
    if (router.query.type && fromQueryVehicleType.length === 0) {
      const newTypes = getFromQueryVehicleType(allowedList, router.query.type);
      setFromQueryVehicleType(newTypes);
    }
  }, [router.query.type]);

  return { fromQueryVehicleType, onChangeVehicleType };
};
