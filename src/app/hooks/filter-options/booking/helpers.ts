export const getCorrectValue = (value: string | number): 1 | 0 => {
  const BOOKING_ENABLED = 1;
  const BOOKING_DISABLED = 0;

  const inputNumber = Number(value);

  if (isNaN(inputNumber)) {
    return BOOKING_ENABLED;
  }

  return inputNumber === BOOKING_DISABLED ? BOOKING_DISABLED : BOOKING_ENABLED;
};
