import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { SelectOption } from 'components/form/InputSelect';
import { getCorrectValue } from 'app/hooks/filter-options/booking/helpers';

const options: SelectOption[] = [
  {
    label: 'Ano',
    value: 1,
  },
  {
    label: 'Ne',
    value: 0,
  },
];

export const useBooking = () => {
  const router = useRouter();
  const [isBooking, setIsBooking] = useState<1 | 0>();

  const changeQueryParams = (option: { isbooking: number }) => {
    router.push(
      {
        pathname: '/',
        query: { ...router.query, ...option },
      },
      undefined,
      { shallow: true }
    );
  };

  const onChangeBooking = ({ value }: SelectOption) => {
    setIsBooking(() => {
      const newValue = getCorrectValue(value);
      changeQueryParams({ isbooking: newValue });

      return newValue;
    });
  };

  useEffect(() => {
    const isBookingQuery = router.query.isbooking;

    if (isBookingQuery != null && isBooking == null) {
      const newValue = getCorrectValue(isBookingQuery);
      setIsBooking(newValue);
    }
  }, [router.query.isbooking]);

  const currentValue =
    options.find((option) => option.value === isBooking) ?? options[0];

  return { onChangeBooking, options, currentValue };
};
