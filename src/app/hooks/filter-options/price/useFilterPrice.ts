import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import {
  getCorrectInputValues,
  getFromQueryRange,
} from 'app/hooks/filter-options/price/helpers';
import { usePrice } from 'app/hooks/usePrice';
import { PriceRange } from 'types/filter';

export const useFilterPrice = () => {
  const { range: fromApiRange } = usePrice();

  const router = useRouter();

  const [fromQueryRange, setFromQueryRange] = useState<number[] | undefined>();

  const changeQueryParams = (inputRange: PriceRange) => {
    const { min, max } = inputRange;

    router.push(
      {
        pathname: '/',
        query: { ...router.query, price: [min, max] },
      },
      undefined,
      { shallow: true }
    );
  };

  const onChangeRange = (inputRange: Partial<PriceRange>) => {
    let { min: minInput, max: maxInput } = inputRange;
    if (fromQueryRange) {
      minInput = minInput ?? fromQueryRange[0];

      if (!maxInput) {
        maxInput = fromQueryRange.length !== 2 ? maxInput : fromQueryRange[1];
      }
    }

    const { min, max } = getCorrectInputValues(
      { min: minInput, max: maxInput },
      fromApiRange
    );

    setFromQueryRange(() => {
      const newState = [min, max];
      changeQueryParams({ min, max });

      return newState;
    });
  };

  useEffect(() => {
    if (router.query.price && !fromQueryRange) {
      const newRange = getFromQueryRange(router.query.price);
      setFromQueryRange(newRange);
    }
  }, [router.query.price]);

  return { fromApiRange, fromQueryRange, onChangeRange };
};
