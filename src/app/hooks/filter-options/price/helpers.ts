import { getAsArray } from 'common/helpers/get-as-array';
import { PriceRange } from 'types/filter';

export const getFromQueryRange = (
  params?: string | string[]
): number[] | undefined => {
  if (params) {
    const priceParamsRange = getAsArray(params)
      .map(Number)
      .filter(Boolean)
      .slice(0, 2);

    if (priceParamsRange.length > 0) {
      return priceParamsRange;
    }
  }
};

export const isInRange = (value: number, requiredRange: PriceRange) => {
  const { min, max } = requiredRange;

  return value >= min && value <= max;
};

export const getCorrectInputValues = (
  inputRange: Partial<PriceRange>,
  defaultRange: PriceRange
): PriceRange => {
  let { min = defaultRange.min, max = defaultRange.max } = inputRange;

  if (!isInRange(min, defaultRange)) {
    min = defaultRange.min;
  }

  if (!isInRange(max, defaultRange)) {
    max = defaultRange.max;
  }

  if (min > max) {
    min = max;
  }

  return { min, max };
};
