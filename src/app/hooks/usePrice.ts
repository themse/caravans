import { CaravanType } from 'api/types/caravan';
import { useCaravanList } from 'app/hooks/useCaravanList';
import { PriceRange } from 'types/filter';

export const getMinMaxPrice = (priceList: number[]): PriceRange => {
  if (priceList.length === 0) {
    return {
      min: 0,
      max: 0,
    };
  }
  return {
    min: Math.min(...priceList),
    max: Math.max(...priceList),
  };
};

export const getRange = (data: CaravanType[]) => {
  const priceList = data.map((caravan) => caravan.price);

  return getMinMaxPrice(priceList);
};

export const usePrice = () => {
  const { data: caravanList } = useCaravanList();

  const range = getRange(caravanList);

  return { range };
};
