import { VehicleType } from 'api/types/vehicle';
import { getAsArray } from 'common/helpers/get-as-array';
import { getAllowedList } from 'app/hooks/filter-options/type/helpers';
import { FilterArgsType } from './types';

export const filterByType = ({
  data,
  filterOptions,
}: FilterArgsType): FilterArgsType => {
  const { type } = filterOptions;
  if (!type) {
    return { data, filterOptions };
  }
  const allowedList = getAllowedList(data);

  const typeQuery = getAsArray(type).filter((item) =>
    allowedList.includes(item)
  );

  return {
    data: data.filter((item) =>
      typeQuery.includes(
        item.vehicleType.toLowerCase() as Lowercase<VehicleType>
      )
    ),
    filterOptions,
  };
};
