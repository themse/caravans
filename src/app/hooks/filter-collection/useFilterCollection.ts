import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { useRouter } from 'next/router';

import { CaravanType } from 'api/types/caravan';
import { QueryKeys } from 'api/types/query-keys';
import { getAllCaravans } from 'api/calls/getAllCaravans';
import { filterByType } from 'app/hooks/filter-collection/filter-by-type';
import { filterByBooking } from 'app/hooks/filter-collection/filter-by-booking';
import { filterByPrice } from 'app/hooks/filter-collection/filter-by-price';
import { FilterArgsType } from 'app/hooks/filter-collection/types';
import { compose } from 'common/helpers/compose';

const COLLECTION_LIMIT = 5;
const COLLECTION_OFFSET = 5;

type FilterCollectionProps = {
  items: CaravanType[];
  count: number;
};

type FilterCollectionReturn = FilterCollectionProps & {
  onLoadMore: () => void;
  isMore: boolean;
};

export const useFilterCollection = (
  initialData: FilterCollectionProps
): FilterCollectionReturn => {
  const router = useRouter();
  const [items, setItems] = useState<CaravanType[]>([]);
  const [count, setCount] = useState(COLLECTION_LIMIT);
  const [isMore, setIsMore] = useState(false);

  const { data } = useQuery(QueryKeys.CARAVAN_LIST, getAllCaravans, {
    initialData,
  });

  const onLoadMore = () => {
    if (count + COLLECTION_OFFSET > initialData.count) {
      return setCount(initialData.count);
    }
    if (count <= initialData.count) {
      return setCount((prev) => prev + COLLECTION_OFFSET);
    }
  };

  useEffect(() => {
    setCount(COLLECTION_LIMIT);
  }, [router.query]);

  useEffect(() => {
    if (data) {
      const { data: filteredItems } = compose<
        (args: FilterArgsType) => FilterArgsType,
        FilterArgsType
      >(
        filterByBooking,
        filterByType,
        filterByPrice
      )({ data: data.items, filterOptions: router.query });

      setIsMore(filteredItems.length > count);
      setItems(filteredItems.slice(0, count));
    }
  }, [router.query, data, count]);

  return { items, count, isMore, onLoadMore };
};
