import { CaravanType } from 'api/types/caravan';
import { FilterOptions } from 'types/filter';

export type FilterArgsType = {
  data: CaravanType[];
  filterOptions: FilterOptions;
};
