import {
  getFromQueryRange,
  isInRange,
} from 'app/hooks/filter-options/price/helpers';
import { getRange } from 'app/hooks/usePrice';
import { FilterArgsType } from 'app/hooks/filter-collection/types';

export const filterByPrice = ({
  data,
  filterOptions,
}: FilterArgsType): FilterArgsType => {
  const defaultRange = getRange(data);

  const { price: priceQuery } = filterOptions;
  if (!priceQuery) {
    return { data, filterOptions };
  }

  const inputRange = getFromQueryRange(priceQuery);

  if (!inputRange) {
    return { data, filterOptions };
  }

  const minInput = inputRange[0] ?? defaultRange.min;
  const maxInput = inputRange[1] ?? defaultRange.max;

  if (isInRange(minInput, defaultRange) && isInRange(maxInput, defaultRange)) {
    return {
      data: data.filter((item) =>
        isInRange(item.price, { min: minInput, max: maxInput })
      ),
      filterOptions,
    };
  }

  return { data, filterOptions };
};
