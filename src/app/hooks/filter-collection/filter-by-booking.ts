import { getAsString } from 'common/helpers/get-as-string';
import { getCorrectValue } from 'app/hooks/filter-options/booking/helpers';
import { FilterArgsType } from './types';

export const filterByBooking = ({
  data,
  filterOptions,
}: FilterArgsType): FilterArgsType => {
  const { isbooking: isBookingQuery = '1' } = filterOptions;

  const isBooking = getCorrectValue(getAsString(isBookingQuery));

  return {
    data: data.filter((item) => item.instantBookable === Boolean(isBooking)),
    filterOptions,
  };
};
