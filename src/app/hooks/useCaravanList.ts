import { useQueryClient } from 'react-query';

import { CaravanType } from 'api/types/caravan';
import { QueryKeys } from 'api/types/query-keys';

type CaravanListReturn = {
  data: CaravanType[];
};

export const useCaravanList = (): CaravanListReturn => {
  const queryClient = useQueryClient();

  const data = queryClient.getQueryData<{ items: CaravanType[] }>(
    QueryKeys.CARAVAN_LIST
  );

  const caravanList = data?.items ?? [];

  return { data: caravanList };
};
