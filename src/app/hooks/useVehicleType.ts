import { VehicleType } from 'api/types/vehicle';
import { useCaravanList } from 'app/hooks/useCaravanList';
import { getAllowedList } from './filter-options/type/helpers';

type CaravanInfoType = {
  title: VehicleType;
  description: string;
};

const caravansInfo: CaravanInfoType[] = [
  {
    title: 'Campervan',
    description: 'Obytka s rozměry osobáku, se kterou dojedete všude.',
  },
  { title: 'Intergrated', description: 'Král mezi karavany. Luxus na kolech.' },
  {
    title: 'BuiltIn',
    description: 'Celý byt geniálně poskládaný do dodávky.',
  },
  {
    title: 'Alcove',
    description: 'Tažný karavan za vaše auto. Od kapkovitých až po rodinné.',
  },
];

type VehicleTypeApiReturn = {
  data: CaravanInfoType[];
  typeList: Lowercase<VehicleType>[];
};

export const useVehicleType = (): VehicleTypeApiReturn => {
  const { data: caravanList } = useCaravanList();

  const vehicleList = getAllowedList(caravanList);

  const caravansType = caravansInfo.filter((item) =>
    vehicleList.includes(item.title.toLowerCase())
  );

  const typeList = vehicleList as Lowercase<VehicleType>[];

  return {
    data: caravansType,
    typeList,
  };
};
