import { FC } from 'react';
import Head from 'next/head';

import { StyledMain } from 'app/components/StyledMain';
import { StyledWrapper } from 'app/components/StyledWrapper';
import { Header } from './Header';

type BaseLayoutProps = {
  title?: string;
};

export const BaseLayout: FC<BaseLayoutProps> = ({
  children,
  title = 'Home',
}) => {
  return (
    <>
      <Head>
        <title>{title} | Prague Labs</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Caravan list" />
        <meta name="keywords" content="caravans" />
      </Head>
      <StyledWrapper>
        <Header />
        <StyledMain>{children}</StyledMain>
      </StyledWrapper>
    </>
  );
};
