import React from 'react';
import styled from 'styled-components';

import { Container } from 'components/Container';
import { Logo } from 'components/Logo';
import { pxToRem } from 'common/styles/helpers';

const StyledHeader = styled.header`
  padding: ${pxToRem(20)} 0;

  @media screen and (max-width: ${({ theme }) => theme.screens.mobile}) {
    margin: 0 auto;
  }
`;

export const Header = () => {
  return (
    <StyledHeader>
      <Container>
        <Logo />
      </Container>
    </StyledHeader>
  );
};
