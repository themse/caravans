import { FC } from 'react';
import styled from 'styled-components';

import { Card } from 'components/Card/Card';
import { pxToRem } from 'common/styles/helpers';
import { CaravanType } from 'api/types/caravan';
import { Button } from 'components/Button';

type CardListProps = {
  items: CaravanType[];
  isMore: boolean;
  onLoadMore: () => void;
};

export const CardList: FC<CardListProps> = ({ items, isMore, onLoadMore }) => {
  return (
    <Wrapper>
      <WrapperList>
        {items.map((item, idx) => (
          <Card
            key={`${item.name}-${idx}`}
            imgSrc={item.pictures[0]} // TODO slider
            name={item.name}
            location={item.location}
            vehicleType={item.vehicleType}
            isBookable={item.instantBookable}
            price={item.price}
            serviceOptions={{
              passengersCapacity: item.passengersCapacity,
              sleepCapacity: item.sleepCapacity,
              hasToilet: item.toilet,
              hasShower: item.shower,
            }}
          />
        ))}
      </WrapperList>
      {isMore && <Button onClick={onLoadMore}>Načíst další</Button>}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${pxToRem(32)};

  ${Button} {
    margin: 0 auto;
    cursor: pointer;
  }
`;

const WrapperList = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: ${pxToRem(32)};

  @media screen and (max-width: 1060px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (max-width: 718px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;
