import styled from 'styled-components';

import { pxToRem } from 'common/styles/helpers';

export const StyledWrapper = styled.div`
  min-height: 100vh;
  min-width: ${({ theme }) => theme.screens.mobile};
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  padding-top: 2px;
  padding-bottom: 2px;
  margin-bottom: ${pxToRem(115)};
`;
