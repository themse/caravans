import { FC } from 'react';
import styled from 'styled-components';

import { InputSelect } from 'components/form/InputSelect';
import { Label } from 'components/form/Label';
import { WarningIcon } from 'components/WarningIcon';
import { useBooking } from 'app/hooks/filter-options/booking/useBooking';

export const FilterBooking: FC = () => {
  const { options, currentValue, onChangeBooking } = useBooking();

  return (
    <Wrapper>
      <Heading>
        <Label htmlFor="select-booking">Okamžitá rezervace</Label>
        <WarningIcon />
      </Heading>
      <InputSelect
        name="booking"
        value={currentValue}
        options={options}
        defaultValue={options[0]}
        id="select-booking"
        onChange={onChangeBooking}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 20px 0;
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

const Heading = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
`;
