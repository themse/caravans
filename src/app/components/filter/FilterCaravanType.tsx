import { FC } from 'react';
import styled from 'styled-components';

import { Label } from 'components/form/Label';
import { InputCheckbox } from 'components/form/InputCheckbox';
import { useVehicleType } from 'app/hooks/useVehicleType';
import {
  FieldNameType,
  useFilterVehicleType,
} from 'app/hooks/filter-options/type/useFilterVehicleType';

export const FilterCaravanType: FC = () => {
  const { data: caravansType } = useVehicleType();
  const { fromQueryVehicleType, onChangeVehicleType } = useFilterVehicleType();

  return (
    <Wrapper>
      <Label>Typ karavanu</Label>
      <OptionsList>
        {caravansType.map(({ title, description }) => {
          const name = title.toLowerCase() as FieldNameType;

          return (
            <InputCheckbox
              key={title}
              title={title}
              name={name}
              description={description}
              onChange={onChangeVehicleType}
              isChecked={fromQueryVehicleType.includes(name)}
            />
          );
        })}
      </OptionsList>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  border-left: 1px solid ${({ theme }) => theme.colors.beige};
  border-right: 1px solid ${({ theme }) => theme.colors.beige};
  padding: 20px 16px;
  display: flex;
  flex-direction: column;
  gap: 16px;

  @media screen and (max-width: 715px) {
    border: none;
    padding: 0;
  }
`;

const OptionsList = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 16px;

  @media screen and (max-width: 1120px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (max-width: 715px) {
    grid-template-columns: repeat(2, min-content);
  }
`;
