import React from 'react';
import styled from 'styled-components';

import { Container } from 'components/Container';
import { FilterPrice } from 'app/components/filter/FilterPrice';
import { FilterCaravanType } from 'app/components/filter/FilterCaravanType';
import { FilterBooking } from 'app/components/filter/FilterBooking';

export const Filter = () => {
  return (
    <Section>
      <Container>
        <Wrapper>
          <FilterPrice />
          <FilterCaravanType />
          <FilterBooking />
        </Wrapper>
      </Container>
    </Section>
  );
};

const Section = styled.section`
  border-top: 1px solid ${({ theme }) => theme.colors.beige};
  border-bottom: 1px solid ${({ theme }) => theme.colors.beige};
  margin-bottom: 32px;
`;

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 3fr 1fr;
  gap: 16px;

  @media screen and (max-width: 1120px) {
    grid-template-columns: 2fr 2fr 1fr;
  }

  @media screen and (max-width: 715px) {
    grid-template-columns: 1fr;
  }
`;
