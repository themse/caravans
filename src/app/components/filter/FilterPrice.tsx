import { FC, useCallback } from 'react';
import styled from 'styled-components';
import debounce from 'lodash.debounce';

import { InputSlider } from 'components/form/InputSlider';
import { InputPrice } from 'components/form/InputPrice';
import { Label } from 'components/form/Label';
import { useFilterPrice } from 'app/hooks/filter-options/price/useFilterPrice';
import { PriceRange } from 'types/filter';

export const FilterPrice: FC = () => {
  const {
    fromApiRange: baseRange,
    fromQueryRange: inputRange,
    onChangeRange,
  } = useFilterPrice();

  const getValidationRules = useCallback(
    (value: number): boolean => {
      return value >= baseRange.min && value <= baseRange.max;
    },
    [baseRange.min, baseRange.max]
  );

  const onChangeMin = (value: number) => {
    onChangeRange({ min: value });
  };
  const onChangeMax = (value: number) => {
    onChangeRange({ max: value });
  };

  const onSliderChange = ({ min, max }: PriceRange) => {
    onChangeRange({ min, max });
  };

  const debouncedSliderChange = useCallback(debounce(onSliderChange, 300), []);

  return (
    <Wrapper>
      <Label>Cena za den</Label>
      <SliderWrapper>
        <InputSlider
          value={inputRange}
          baseRange={baseRange}
          defaultValue={baseRange}
          onSliderChange={debouncedSliderChange}
        />
      </SliderWrapper>
      <InputGroup>
        <InputPrice
          value={inputRange ? inputRange[0] : baseRange.min}
          defaultValue={baseRange.min}
          currency="Kč"
          onChange={onChangeMin}
          getValidationRules={getValidationRules}
        />
        <InputPrice
          value={inputRange ? inputRange[1] : baseRange.max}
          defaultValue={baseRange.max}
          currency="Kč"
          onChange={onChangeMax}
          getValidationRules={getValidationRules}
        />
      </InputGroup>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 20px 0;
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

const SliderWrapper = styled.div`
  padding: 4px 12px;
`;

const InputGroup = styled.div`
  display: flex;
  gap: 16px;

  @media screen and (max-width: 1120px) {
    flex-direction: column;
    gap: 10px;
  }

  @media screen and (max-width: 715px) {
    flex-direction: row;
    gap: 16px;
  }
`;
