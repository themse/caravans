import { axios } from 'common/axios';

import { CaravanType } from 'api/types/caravan';

type ResponseType = {
  count: number;
  items: CaravanType[];
};

export const getAllCaravans = async (): Promise<ResponseType> => {
  const { data } = await axios.get('/data');

  return data;
};
