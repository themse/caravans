import { FC } from 'react';
import {
  QueryClient,
  QueryClientProvider as LibQueryClientProvider,
} from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

const queryClient = new QueryClient();

export const QueryClientProvider: FC = ({ children }) => {
  return (
    <LibQueryClientProvider client={queryClient}>
      {children}
      <ReactQueryDevtools initialIsOpen={false} />
    </LibQueryClientProvider>
  );
};
