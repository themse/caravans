import { VehicleType } from 'api/types/vehicle';

export type CaravanType = {
  location: string;
  instantBookable: boolean;
  name: string;
  passengersCapacity: number;
  sleepCapacity: number;
  price: number;
  vehicleType: VehicleType;
  toilet: boolean;
  shower: boolean;
  pictures: string[];
};
