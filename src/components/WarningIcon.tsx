import styled from 'styled-components';

import { Icon } from 'components/Icon';

export const WarningIcon = styled(Icon).attrs(() => ({
  title: 'Warning Icon',
  src: '/icons/icon-power.svg',
}))`
  width: 16px;
  height: 16px;
`;
