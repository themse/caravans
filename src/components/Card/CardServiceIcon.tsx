import { FC } from 'react';
import Image from 'next/image';

import { ServiceOptions } from 'components/Card/CardServiceList';

type CardServiceIconProps = {
  type: keyof ServiceOptions;
  src: string;
};

export const CardServiceIcon: FC<CardServiceIconProps> = ({ type, src }) => {
  return (
    <Image
      alt={type}
      src={src}
      width={20}
      height={20}
      layout="fixed"
      quality={45}
    />
  );
};

CardServiceIcon.displayName = 'CardServiceIcon';
