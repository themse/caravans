import { FC } from 'react';
import styled from 'styled-components';

import { WarningIcon } from 'components/WarningIcon';

type CardPriceProps = {
  price: number;
  isBookable: boolean;
};

export const CardPrice: FC<CardPriceProps> = ({ price, isBookable }) => {
  return (
    <Wrapper>
      <Label>Cena od</Label>
      <PriceWrapper>
        <Price>{`${price} Kč/den`}</Price>
        {isBookable && <WarningIcon />}
      </PriceWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 15px 0 5px;
`;

const Label = styled.p`
  color: ${({ theme }) => theme.colors.darkGrey};
`;

const Price = styled.p`
  color: ${({ theme }) => theme.colors.darkBlue};
  ${({ theme }) => theme.fontFace.heading};
`;

const PriceWrapper = styled.div`
  display: flex;

  ${WarningIcon} {
    margin-left: 8px;
  }
`;
