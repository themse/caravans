import { FC } from 'react';
import styled from 'styled-components';

import { ObjectEntries } from 'types/helpers';
import { CardServiceIcon } from 'components/Card/CardServiceIcon';
import { CardServiceItem } from 'components/Card/CardServiceItem';

export type ServiceOptions = {
  passengersCapacity: number;
  sleepCapacity: number;
  hasToilet: boolean;
  hasShower: boolean;
};

type CardServiceListProps = {
  location: string;
  options: ServiceOptions;
};

const serviceIcons = {
  passengersCapacity: (
    <CardServiceIcon
      type="passengersCapacity"
      src="/icons/icon-passenger.svg"
    />
  ),
  sleepCapacity: (
    <CardServiceIcon type="passengersCapacity" src="/icons/icon-bed.svg" />
  ),
  hasToilet: (
    <CardServiceIcon type="passengersCapacity" src="/icons/icon-toilet.svg" />
  ),
  hasShower: (
    <CardServiceIcon type="passengersCapacity" src="/icons/icon-bathroom.svg" />
  ),
};

export const CardServiceList: FC<CardServiceListProps> = ({
  location,
  options,
}) => {
  return (
    <Wrapper>
      <Location>{location}</Location>

      {(Object.entries(options) as ObjectEntries<ServiceOptions>)
        .filter(([_, value]) => value !== false)
        .map(([type, value]) => (
          <CardServiceItem key={type} value={value}>
            {serviceIcons[type]}
          </CardServiceItem>
        ))}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.beige};
  margin-bottom: 5px;
  padding: 15px 0;
  display: flex;
  gap: 10px;
`;

const Location = styled.p`
  color: ${({ theme }) => theme.colors.darkBlue};
  font-size: ${({ theme }) => theme.fontSize.md};
`;
