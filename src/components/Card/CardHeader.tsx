import { FC } from 'react';
import styled from 'styled-components';

type CardHeaderProps = {
  title: string;
  vehicleType: string;
};

export const CardHeader: FC<CardHeaderProps> = ({ title, vehicleType }) => {
  return (
    <Header>
      <VehicleType>{vehicleType}</VehicleType>
      <Title>{title}</Title>
    </Header>
  );
};

const Header = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.beige};
`;

const VehicleType = styled.p`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSize.sm};
  ${({ theme }) => theme.fontFace.heading};
  text-transform: uppercase;
`;

const Title = styled.h2`
  color: ${({ theme }) => theme.colors.darkBlue};
  font-size: ${({ theme }) => theme.fontSize.xl};
  ${({ theme }) => theme.fontFace.heading};
  margin: 10px 0;
`;
