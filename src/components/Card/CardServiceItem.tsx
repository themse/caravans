import { FC } from 'react';
import styled from 'styled-components';

type CardServiceItemProps = {
  value: number | boolean;
};

export const CardServiceItem: FC<CardServiceItemProps> = ({
  value,
  children,
}) => {
  const isAmount = typeof value === 'number';

  return (
    <Wrapper>
      {children}
      {isAmount && <Amount>{value}</Amount>}
    </Wrapper>
  );
};

const Amount = styled.p`
  color: ${({ theme }) => theme.colors.darkBlue};
  font-size: ${({ theme }) => theme.fontSize.md};
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
`;
