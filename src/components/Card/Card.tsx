import { FC } from 'react';
import Image from 'next/image';
import styled from 'styled-components';

import { CardHeader } from 'components/Card/CardHeader';
import { CardPrice } from 'components/Card/CardPrice';
import {
  CardServiceList,
  ServiceOptions,
} from 'components/Card/CardServiceList';

const BLUR_DATA_URL =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOc09NTDwAFtQI10JeDVAAAAABJRU5ErkJggg==';

type CardProps = {
  imgSrc: string;
  name: string;
  vehicleType: string;
  location: string;
  price: number;
  isBookable: boolean;
  serviceOptions: ServiceOptions;
};

export const Card: FC<CardProps> = ({
  imgSrc,
  name,
  location,
  isBookable,
  serviceOptions,
  price,
  vehicleType,
}) => {
  return (
    <Wrapper>
      <ImageThumbnail>
        <Image
          unoptimized // TODO rethink about remote img
          alt={name}
          src={imgSrc}
          layout="fill"
          objectFit="cover"
          objectPosition="center"
          placeholder="blur"
          blurDataURL={BLUR_DATA_URL}
        />
      </ImageThumbnail>
      <Content>
        <CardHeader title={name} vehicleType={vehicleType} />
        <CardServiceList location={location} options={serviceOptions} />
        <CardPrice price={price} isBookable={isBookable} />
      </Content>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.beige};
  border-radius: 8px;
  overflow: hidden;
  min-width: 320px;
`;

const ImageThumbnail = styled.div`
  position: relative;
  height: 190px;
`;

const Content = styled.div`
  padding: 12px 16px;
`;
