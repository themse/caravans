import styled from 'styled-components';

export const Container = styled.div`
  max-width: ${({ theme }) => theme.screens.desktop};
  margin: 0 auto;

  @media screen and (max-width: ${({ theme }) => theme.screens.desktop}) {
    padding: 0 15px;
  }
`;
