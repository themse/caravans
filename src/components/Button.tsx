import styled from 'styled-components';

export const Button = styled.button.attrs(() => ({
  type: 'button',
}))`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.green};
  border-radius: 8px;
  padding: 13px 35px;
  box-shadow: none;
  border: none;
  display: inline-block;
`;
