import { FC } from 'react';
import Image from 'next/image';
import styled from 'styled-components';

export const Logo: FC = () => {
  return (
    <Wrapper>
      <Image src="/logo.svg" alt="Company Logo" layout="fill" priority />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 2px 0;
  width: 200px;
  height: 35px;
  position: relative;
`;
