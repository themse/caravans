import { FC } from 'react';
import styled from 'styled-components';

export const Alert: FC = ({ children }) => {
  return (
    <Wrapper>
      <Message>{children}</Message>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.orange};
  color: ${({ theme }) => theme.colors.beige};
  padding: 1rem;
  border-radius: 8px;
`;

const Message = styled.p`
  text-align: center;
`;
