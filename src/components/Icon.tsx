import { FC, ObjectHTMLAttributes } from 'react';
import styled from 'styled-components';

type IconProps = ObjectHTMLAttributes<HTMLObjectElement> & {
  title: string;
  src: string;
};

export const Icon: FC<IconProps> = ({ title, src, ...props }) => {
  return (
    <StyledObject
      type="image/svg+xml"
      data={src}
      aria-label={title}
      {...props}
    />
  );
};

const StyledObject = styled.object`
  pointer-events: none;
`;
