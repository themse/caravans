import { FC, ChangeEvent, FocusEvent, useState, useEffect } from 'react';
import styled from 'styled-components';

import { numberWithSpaces } from 'common/helpers/number-with-spaces';

type InputPriceProps = {
  value: number;
  defaultValue: number;
  currency: string;

  getValidationRules: (value: number) => boolean;
  onChange?: (value: number) => void;
};

export const InputPrice: FC<InputPriceProps> = ({
  value: inputValue,
  currency,
  defaultValue,
  getValidationRules,
  onChange: onChangeProps,
}) => {
  const [value, setValue] = useState<string>(() =>
    numberWithSpaces(defaultValue)
  );

  const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const eventValue = Number(event.target.value || defaultValue);
    if (isNaN(eventValue) || eventValue < 0) {
      return;
    }
    setValue(event.target.value);
  };

  const onFocus = (event: FocusEvent<HTMLInputElement>) => {
    const newValue = event.target.value.split(' ').join('');

    setValue(newValue);
  };

  const onBlur = (event: FocusEvent<HTMLInputElement>) => {
    const eventValue = Number(event.target.value || defaultValue);
    const newValue = getValidationRules(eventValue) ? eventValue : defaultValue;

    onChangeProps && onChangeProps(newValue);
    setValue(numberWithSpaces(newValue));
  };

  useEffect(() => {
    if (inputValue) {
      const newValue = numberWithSpaces(inputValue);
      setValue(newValue);
    }
  }, [inputValue]);

  return (
    <Wrapper>
      <StyledInput
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        onFocus={onFocus}
      />
      <Currency>{currency}</Currency>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  position: relative;
`;

const StyledInput = styled.input.attrs(() => ({
  type: 'text',
}))`
  width: 100%;
  padding: 14px 40px 14px 12px;
  color: ${({ theme }) => theme.colors.darkBlue};
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.beige};
  border-radius: 8px;

  &:focus {
    outline: none;
    border: 2px solid ${({ theme }) => theme.colors.green};
    padding: 13px 39px 13px 11px;
  }
`;

const Currency = styled.span`
  position: absolute;
  right: 12px;
  z-index: 2;
  color: ${({ theme }) => theme.colors.darkGrey};
`;
