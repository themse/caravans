import { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import { PriceRange } from 'types/filter';
import { getAsArray } from 'common/helpers/get-as-array';

type InputSliderProps = {
  baseRange: PriceRange;
  defaultValue: PriceRange;
  value?: number[];

  onSliderChange: (value: PriceRange) => void;

  [key: string]: unknown;
};

export const InputSlider: FC<InputSliderProps> = ({
  value,
  baseRange,
  defaultValue,
  onSliderChange,
  ...props
}) => {
  const { min, max } = baseRange;

  const [inputMin, setInputMin] = useState(min);
  const [inputMax, setInputMax] = useState(max);

  const onChange = (value: number[] | number) => {
    const [minVal, maxVal] = getAsArray<number>(value);
    setInputMin(minVal);
    setInputMax(maxVal);

    onSliderChange({ min: minVal, max: maxVal });
  };

  useEffect(() => {
    if (value) {
      const [minVal, maxVal] = value;
      setInputMin(minVal);
      setInputMax(maxVal ?? max);
    }
  }, [max, value]);

  return (
    <StyledSlider
      value={[inputMin, inputMax]}
      defaultValue={[defaultValue.min, defaultValue.max]}
      min={min}
      max={max}
      onChange={onChange}
      allowCross={false}
      range
      {...props}
    />
  );
};

const StyledSlider = styled(Slider)`
  .rc-slider-handle {
    background-color: ${({ theme }) => theme.colors.green};
    border: none;
    opacity: 1;
    width: 24px;
    height: 24px;
    margin-top: -10px;
  }

  .rc-slider-track {
    background-color: ${({ theme }) => theme.colors.green};
  }

  .rc-slider-rail {
    background-color: ${({ theme }) => theme.colors.beige};
  }
`;
