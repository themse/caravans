import { FC } from 'react';
import styled from 'styled-components';
import Select, { ActionMeta } from 'react-select';

export type SelectOption = {
  label: string;
  value: number;
};

type InputSelectProps = {
  name: string;
  defaultValue: SelectOption;
  options: SelectOption[];
  onChange: (value: SelectOption) => void;

  [key: string]: unknown;
};

export const InputSelect: FC<InputSelectProps> = ({
  name,
  defaultValue,
  options,
  onChange: onChangeProps,
  ...props
}) => {
  const onChange = (value: unknown, _actionMeta?: ActionMeta<unknown>) => {
    onChangeProps(value as SelectOption);
  };

  return (
    <StyledSelect
      classNamePrefix="react-select"
      name={name}
      options={options}
      defaultValue={defaultValue}
      hideSelectedOptions
      components={{
        IndicatorSeparator: () => null,
      }}
      onChange={onChange}
      isSearchable={false}
      {...props}
    />
  );
};

const StyledSelect = styled(Select)`
  .react-select {
    &__control {
      border-color: ${({ theme }) => theme.colors.beige};
      border-radius: 8px;
      max-width: 175px;

      &:hover {
        border-color: ${({ theme }) => theme.colors.beige};
      }

      &--is-focused {
        box-shadow: none;
        border: 2px solid ${({ theme }) => theme.colors.green} !important;

        .react-select__indicator {
          color: ${({ theme }) => theme.colors.green};
          padding: 12px 7px 12px 8px;
        }

        .react-select__value-container {
          padding-left: 7px;
        }
      }
    }

    &__indicator {
      color: ${({ theme }) => theme.colors.beige};
      padding: 13px 8px;
    }

    &__menu {
      box-shadow: none;
      margin-top: 5px;
    }

    &__menu-list {
      padding-top: 0;
      padding-bottom: 0;
    }

    &__option {
      background: none;
      border-bottom-left-radius: 8px;
      border-bottom-right-radius: 8px;
      border: 1px solid ${({ theme }) => theme.colors.beige};
      max-width: 175px;
    }
  }
`;
