import { FC, ChangeEvent } from 'react';
import styled from 'styled-components';

type InputCheckboxProps = {
  title: string;
  description: string;

  onChange: ({ name, isChecked }: { name: string; isChecked: boolean }) => void;
  name?: string;
  isChecked?: boolean;
};

export const InputCheckbox: FC<InputCheckboxProps> = ({
  title,
  description,
  name: nameInput,
  onChange: onChangeProps,

  isChecked = false,
}) => {
  const name = nameInput ?? title.toLowerCase();

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChangeProps({ name, isChecked: event.target.checked });
  };

  return (
    <>
      <HiddenCheckbox id={title} onChange={onChange} checked={isChecked} />
      <LabelWrapper htmlFor={title}>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </LabelWrapper>
    </>
  );
};

const LabelWrapper = styled.label`
  width: 155px;
  border-radius: 8px;
  border: 1px solid ${({ theme }) => theme.colors.beige};
  padding: 11px 12px;
  cursor: pointer;
`;

const HiddenCheckbox = styled.input.attrs(() => ({
  type: 'checkbox',
}))`
  display: none;

  &:checked + ${LabelWrapper} {
    border: 2px solid ${({ theme }) => theme.colors.green};
    padding: 10px 11px;
  }
`;

const Title = styled.p`
  color: ${({ theme }) => theme.colors.darkBlue};
  margin-bottom: 4px;
`;

const Description = styled.p`
  color: ${({ theme }) => theme.colors.darkGrey};
  font-size: ${({ theme }) => theme.fontSize.sm};
`;
