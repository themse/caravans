import 'styled-components';

import { FontFace } from 'common/styles/types';

declare module 'styled-components' {
  export interface DefaultTheme {
    screens: {
      mobile: string;
      desktop: string;
    };

    colors: {
      orange: string;
      beige: string;
      white: string;
      darkBlue: string;
      darkGrey: string;
      green: string;
    };

    fontSize: {
      sm: string;
      md: string;
      lg: string;
      xl: string;
    };

    fontFace: {
      primary: FontFace;
      heading: FontFace;
    };
  }
}
