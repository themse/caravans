import { VehicleType } from 'api/types/vehicle';

export type PriceRange = {
  min: number;
  max: number;
};

export type VehicleTypeOption = {
  name: string;
  isChecked: boolean;
};

export type FilterOptions = {
  price?: string | string[];
  type?: Lowercase<VehicleType>[];
  isbooking?: string;
};
