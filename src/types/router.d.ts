import { VehicleType } from 'api/types/vehicle';

declare module 'querystring' {
  export interface ParsedUrlQuery extends NodeJS.Dict<string | string[]> {
    price?: string | string[];
    type?: Lowercase<VehicleType>[];
    isbooking?: string;
  }
}
